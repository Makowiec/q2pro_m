# Q2PRO_M

### What is it
This repository was forked from [skuller's, q2pro](https://github.com/skullernet/q2pro). The main goal of it is to add a bunch of visual improvements and new functionality to the engine.

### Compiling
The code should compile exactly the same way as the original code with one minor difference - the config and makefile are already set up for cross compile with mingw. The mingw SDK still needs to be obtained separately. Detailed instructions can be found [here](https://skuller.net/q2pro/) or in the doc/ subdirectory.

### Feature requests/bugs
Feel free to submit your requests and bug reports via the Issues page. Currently on the list are:

* SSAO
* Screen Space Reflections
* Configurable post processing stack
* Improved particle effects
* In-game chat emojis ;)
* Making .wal textures optional
* Modern 3D model format loading (?)
* Getting rid of some narrow engine limitations

Already implemented or WIP:

* fog
* weapon dropping hotkey
* post processing shader setup

# Q2PRO - original readme

Q2PRO is an enhanced, multiplayer oriented Quake 2 client and server.

Features include:

* rewritten OpenGL renderer optimized for stable FPS
* enhanced client console with persistent history
* ZIP packfiles (.pkz), JPEG and PNG textures, MD3 models
* fast HTTP downloads
* multichannel sound using OpenAL
* recording from demos, forward and backward seeking
* server side multiview demos and GTV capabilities

For building Q2PRO, consult the INSTALL file.

For information on using and configuring Q2PRO, refer to client and server
manuals available in doc/ subdirectory.
